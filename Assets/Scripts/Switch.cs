﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    [SerializeField] private AuxLightController lightController;
    [SerializeField] GameObject pressButtonText;
    [SerializeField] private Transform ghostParent;
    [SerializeField] private Transform switchParent;
    [SerializeField] private Footprint footprint;
    private bool switchInRange = false;

    private List<Vector3[]> switchPathList;
    private int switchIndex=0;
    private bool distanceStored;
    private Coroutine cor = null;

    private void Start()
    {
        switchPathList = new List<Vector3[]>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            if (switchInRange)
            {
                lightController.TurnOff();
                footprint.HidePath();
                pressButtonText.SetActive(false);
                foreach (Transform ghost in ghostParent)
                    ghost.GetComponent<ISoundTrigger>().OnSwitchOff();
            }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Switch"))
        {
            if (lightController.IsOn)pressButtonText.SetActive(true);
            switchInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Switch"))
        {
            pressButtonText.SetActive(false);
            switchInRange = false;
        }
    }

    public void findPathToSwitch()
    {
        if (cor == null && switchParent.childCount>0)
            cor = StartCoroutine(findClosestSwitch());
    }

    IEnumerator findClosestSwitch()
    {
        switchPathList.Clear();
        switchIndex = 0;
        foreach (Transform switchT in switchParent)
        {
            distanceStored = false;
            PathRequestManager.RequestPath(transform.position, switchT.position, storeDistanceToSwitch);
            yield return new WaitUntil(() => distanceStored == true);
            switchIndex++;
        }

        int closest = 0;
        float closestDistance = getPathLenght(switchPathList[0]);
        for (int i = 1; i < switchPathList.Count; i++)
        {
            float currentPathLenght = getPathLenght(switchPathList[i]);
            if (currentPathLenght < closestDistance)
            {
                closest = i;
                closestDistance = currentPathLenght;
            }
        }

        foundClosestSwitch(closest);
    }

    private void storeDistanceToSwitch(Vector3[] path, bool success)
    {
        if (success)
            switchPathList.Add(path);
        distanceStored = true;
    }

    private void foundClosestSwitch(int closestSwitchIndex)
    {
        Debug.Log("found closest switch @" + switchPathList[closestSwitchIndex][switchPathList[closestSwitchIndex].Length - 1]);
        footprint.ShowPath(switchPathList[closestSwitchIndex], transform.position);
        
        StopCoroutine(cor);
        cor = null;
    }

    private float getPathLenght(Vector3[] path)
    {
        float d = Vector3.Distance(transform.position, path[0]);
        for (int i = 0; i < path.Length - 1; i++)
            d += Vector3.Distance(path[i], path[i + 1]);
        return d;
    }

}
