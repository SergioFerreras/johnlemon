﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour
{
    [SerializeField] GameObject hideText;
    [SerializeField] GameObject exitText;
    [SerializeField] AudioSource audio;
    [SerializeField] Collider col;
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] PlayerMovement playerMovement;

    [HideInInspector] bool isNearHidingPlace;
    public bool isInsideHidingPlace;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(isNearHidingPlace)
                hidePlayer();
            else if (isInsideHidingPlace)
                showPlayer();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("HidingPlace"))
        {
            hideText.SetActive(true);
            isNearHidingPlace = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("HidingPlace"))
        {
            hideText.SetActive(false);
            isNearHidingPlace = false;
        }
    }

    void hidePlayer()
    {
        hideText.SetActive(false);
        exitText.SetActive(true);
        meshRenderer.enabled = false;
        playerMovement.enabled = false;
        audio.enabled = false;
        col.enabled = false;
        isNearHidingPlace = false;
        isInsideHidingPlace = true;
    }

    void showPlayer()
    {
        hideText.SetActive(true);
        exitText.SetActive(false);
        meshRenderer.enabled = true;
        playerMovement.enabled = true;
        audio.enabled = true;
        col.enabled = true;
        isNearHidingPlace = true;
        isInsideHidingPlace = false;
    }
}
