﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    const float TIME_BETWEEN_SOUNDS = 0.5f;

    public float turnSpeed = 20f;
    public float walkSpeedMultipler = 0.85f;
    public float runSpeedMultipler = 2;

    public float soundDistance = 5;
    public float soundRunVol = 1f;
    public float soundWalkVol = 0.6f;
    public float soundRunPitch = 1.5f;
    public float soundWalkPitch = 1f;
    bool isMakingSound;

    float soundTimer = 0f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    Hide hide;
    Collider[] collidersCache = new Collider[32];
    LayerMask enemyLayer;
    [SerializeField] Transform rayOrigin;

    void Start()
    {
        hide = GetComponent<Hide>();
        enemyLayer = LayerMask.GetMask("Enemy");
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {

        if (hide.isInsideHidingPlace)
        {
            isMakingSound = false;
            soundTimer = 0f;
        }
        if (isMakingSound)
        {
            soundTimer += Time.deltaTime;
            if(soundTimer >= TIME_BETWEEN_SOUNDS)
            {
                AlertEnemiesOnRange();
                soundTimer = 0f;
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {

            m_AudioSource.volume = soundRunVol;
            m_AudioSource.pitch = soundRunPitch;
            m_Animator.speed = runSpeedMultipler;
            isMakingSound = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            m_AudioSource.volume = soundWalkVol;
            m_AudioSource.pitch = soundWalkPitch;
            m_Animator.speed = walkSpeedMultipler;
            isMakingSound = false;
            soundTimer = 0f;
        }
    }

        void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if(isWalking)
        {
             if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play ();
            }
        }
        else
        {
            m_AudioSource.Stop ();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    public bool isRunnning()
    {
        return m_Animator.speed > 1;
    }

    void AlertEnemiesOnRange()
    {
        int nGhosts = Physics.OverlapSphereNonAlloc(transform.position, soundDistance, collidersCache, enemyLayer);
        for (int i = 0; i < nGhosts; i++)
        {
            if (collidersCache[i] != null && CheckRayCast(collidersCache[i]))
            {
                collidersCache[i].transform.GetComponentInParent<ISoundTrigger>().OnSoundDetected(transform.position, false);
            }
        }
    }

    bool CheckRayCast(Collider ghost)
    {
        Vector3 direction = ghost.transform.position - rayOrigin.position;
        Ray ray = new Ray(rayOrigin.position, direction);
        Debug.DrawRay(rayOrigin.position, direction, Color.red);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        if (hit.collider == ghost)
            return true;
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, soundDistance);
    }
}