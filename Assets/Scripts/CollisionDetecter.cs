﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetecter : MonoBehaviour
{
    [SerializeField] GameEnding gameEnding;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            gameEnding.CaughtPlayer();
    }
}
