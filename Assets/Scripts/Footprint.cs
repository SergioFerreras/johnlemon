﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footprint : MonoBehaviour
{
    public Vector3[] path { get; set; }
    private Coroutine cor;
    private int index = 0;
    private float lerpAlpha = 0;
    [SerializeField] private float stepDist = 0.2f;
    private Vector3 offset;
    [SerializeField] private float y = 0;
    [SerializeField] private float stepTime = 0.5f;
    [SerializeField] private float stepOffsetX = 0.5f;





    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(stepOffsetX, 0,0);
    }

    public void ShowPath(Vector3[] pathToFollow, Vector3 playerPos)
    {
        index = 0;
        lerpAlpha = 0;
        foreach (Transform child in transform)
            child.gameObject.SetActive(true);
        if (cor!=null)
            StopCoroutine(cor);
        path = new Vector3[pathToFollow.Length+1];
        path[0] =playerPos;
        for (int i =0; i<pathToFollow.Length; i++)
            path[i+1] = pathToFollow[i];
        cor = StartCoroutine("displayPathStep");
    }

    public void HidePath()
    {
        if (cor != null)
            StopCoroutine(cor);
        foreach (Transform child in transform)
            child.gameObject.SetActive(false);
    }

    IEnumerator displayPathStep()
    {
        while (true)
        {
            transform.position = Vector3.Lerp(path[index], path[index + 1], Mathf.Clamp(lerpAlpha, 0, 1));
            transform.position = new Vector3(transform.position.x, y, transform.position.z);

            lerpAlpha += stepDist / Vector3.Distance(path[index], path[index + 1]);
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            if (lerpAlpha < 1 || index < path.Length-2)
                transform.LookAt(new Vector3(path[index + 1].x, y, path[index + 1].z));

            transform.position += transform.TransformVector(offset);

            if (lerpAlpha >= 1f)
            {
                lerpAlpha = 0f;
                index++;
                if (index > path.Length - 2)
                    index = 0;
            }

            yield return new WaitForSeconds(stepTime);
        }
    }
}
