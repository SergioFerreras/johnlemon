﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxLightController : MonoBehaviour
{
    [SerializeField]    private float minIntensity=0;
    [SerializeField]    private float maxIntensity=1;
    [SerializeField]    private float minToMaxTime = 1;

    private Light lightSource;

    private float timeElapsed=0;
    private int multiplier = 1;
    public bool IsOn { get; private set; } = false;

    private void Start()
    {
        lightSource = transform.GetComponent<Light>();
        lightSource.intensity = 0;
    }

    void Update()
    {
        if (IsOn)
        {
            timeElapsed += Time.deltaTime * multiplier;
            if (timeElapsed > minToMaxTime || timeElapsed<0)
            {
                timeElapsed = Mathf.Clamp(timeElapsed,0,minToMaxTime);
                multiplier *= -1;
            }
            lightSource.intensity = Mathf.Lerp(minIntensity, maxIntensity, timeElapsed / minToMaxTime);

        }
    }

    public void TurnOn()
    {
        if (IsOn == false)
        {
            IsOn = true;
            //play sound (ADD)
            lightSource.intensity = minIntensity;
        }
    }

    public void TurnOff()
    {
        IsOn = false;
        lightSource.intensity = 0;
    }
}
