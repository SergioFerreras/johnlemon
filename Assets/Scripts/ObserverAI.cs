﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObserverAI : MonoBehaviour
{
    public Transform player;
    public Transform playerRayOrigin;
    public GameEnding gameEnding;
    private int mask;

   public bool m_IsPlayerInRange;

    private void Start()
    {
        mask = LayerMask.GetMask("Unwalkable","Player");
    }

    public bool CheckIfPlayerIsInRange()
    {
        if (m_IsPlayerInRange)
        {
            Vector3 direction = playerRayOrigin.position - transform.position;
            Ray ray = new Ray(transform.position, direction);
            Debug.DrawRay(transform.position,direction,Color.blue,0.05f);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit,Mathf.Infinity,mask))
            {
                Debug.Log(raycastHit.collider.transform.gameObject.name);
                if (raycastHit.collider.transform == player)
                {
                    return true;
                }
            }

        }
        return false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }
}