using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameEnvironment
{
	private static GameEnvironment instance;
	private List<GameObject> waypoints = new List<GameObject>();
	public List<GameObject> Waypoints { get { return waypoints; } }

	public static GameEnvironment Singleton
	{
		get
		{
			if(instance == null)
			{
				instance = new GameEnvironment();
				instance.waypoints.AddRange(GameObject.FindGameObjectsWithTag("Waypoint")); //This can be easly improved with a singleton or scriptable object
				//Problem: FindGameObjectsWithTag won't always give us the waypoints in the same order, if we want to pick them in alphabetical order we could use:
				/*
				using System.Linq;
				
				instance.waypoints = instance.waypoints.OrderBy(waypoint => waypoint.name).ToList();
				*/
			}
			return instance;
		}
	}
}