using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected GhostAI npc;

    public State(GhostAI npc)
    {
        this.npc = npc;
    }

    public abstract void OnStateEnter();

    public abstract void UpdateState();

    public abstract void OnStateExit();

    public abstract void WaypointReached();

}