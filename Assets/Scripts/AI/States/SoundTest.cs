﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[DefaultExecutionOrder(100)]
public class SoundTest : MonoBehaviour
{
    LayerMask layer;
    // Start is called before the first frame update
    void OnEnable()
    {
        layer = LayerMask.GetMask("Enemy");
        Collider[] cols = Physics.OverlapSphere(transform.position, 10, layer);
        foreach (var col in cols)
        {
            col.GetComponent<ISoundTrigger>().OnSoundDetected(transform.position, true);
        }
        
    }

    private void OnDisable()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, 10, layer);
        foreach (var col in cols)
        {
            col.GetComponent<ISoundTrigger>().OnSwitchOff();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 10);
    }
}
