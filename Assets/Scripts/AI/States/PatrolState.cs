﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : State
{

    int currentWaypointIndex = 1;



    public PatrolState(GhostAI npc) : base(npc)
    {
    }

    public override void OnStateEnter()
    {
        PathRequestManager.RequestPath(npc.transform.position, npc.waypointsToPatrol[currentWaypointIndex].position, npc.OnPathFound);
    }

    public override void UpdateState()
    {
        if (npc.isSwitch && !npc.playerHide.isInsideHidingPlace)
        {
            npc.SetState(npc.chasePlayerState);
        }
    }

    public override void OnStateExit()
    {

    }

    public override void WaypointReached()
    {
        currentWaypointIndex++;
        if(currentWaypointIndex >= npc.waypointsToPatrol.Length)
        {
            currentWaypointIndex = 0;
        }
        PathRequestManager.RequestPath(npc.transform.position, npc.waypointsToPatrol[currentWaypointIndex].position, npc.OnPathFound);
    }
}
