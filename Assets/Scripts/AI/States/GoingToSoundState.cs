﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoingToSoundState : State
{
    const float TIME_OF_WAITING = 2f;

    float timer = 0;

    bool waypointReached = false;

    public GoingToSoundState(GhostAI npc) : base(npc)
    {

    }

    public override void OnStateEnter()
    {
        PathRequestManager.RequestPath(npc.transform.position, npc.currentWaypoint, npc.OnPathFound);
    }

    public override void UpdateState()
    {
        if (waypointReached)
        {
            timer += Time.deltaTime;
            if(timer >= TIME_OF_WAITING)
            {
                npc.SetState(npc.patrolState);
            }
        }
    }

    public override void OnStateExit()
    {
        waypointReached = false;
        timer = 0;
    }

    public override void WaypointReached()
    {
        waypointReached = true;
    }
}
