﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayerState : State
{
    const float TIME_BETWEEN_REQUESTS = 0.5f;
    const float TIME_WAITING_ON_HIDE = 2f;

    float requestTimer = 0f;
    float waitingTimer = 0f;

    public ChasePlayerState(GhostAI npc) : base(npc)
    {
    }

    public override void OnStateEnter()
    {
        
        PathRequestManager.RequestPath(npc.transform.position, npc.player.position, npc.OnPathFound);
        npc.GetComponentInChildren<Light>().enabled = true;
    }

    public override void UpdateState()
    {
        if (npc.playerHide.isInsideHidingPlace)
        {
            npc.observer.m_IsPlayerInRange = false;
            waitingTimer += Time.deltaTime;
            if(waitingTimer >= TIME_WAITING_ON_HIDE)
            {
                npc.SetState(npc.patrolState);
            }
        }
        else
        {
            waitingTimer = 0f;
            requestTimer += Time.deltaTime;
            if(requestTimer >= TIME_BETWEEN_REQUESTS)
            {
                PathRequestManager.RequestPath(npc.transform.position, npc.player.position, npc.OnPathFound);
                requestTimer = 0f;
            }
        }
    }

    public override void OnStateExit()
    {
        requestTimer = 0f;
        waitingTimer = 0f;
        npc.GetComponentInChildren<Light>().enabled = false;
    }

    public override void WaypointReached()
    {
        PathRequestManager.RequestPath(npc.transform.position, npc.player.position, npc.OnPathFound);
    }
}
