﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISoundTrigger
{
    void OnSoundDetected(Vector3 position, bool isSwitch);

    void OnSwitchOff();
}
