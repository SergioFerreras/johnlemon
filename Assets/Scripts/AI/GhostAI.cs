using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAI : MonoBehaviour, ISoundTrigger
{
	public Transform[] waypointsToPatrol;
    [SerializeField] private float speed = 5f;
    [SerializeField] private float rotationSpeed = 5f;
    [SerializeField] public ObserverAI observer;

    [HideInInspector] public Hide playerHide;
    [HideInInspector] public Transform player;
    State currentState;

	[HideInInspector] public GoingToSoundState goingToSoundState;
	[HideInInspector] public PatrolState patrolState;
	[HideInInspector] public ChasePlayerState chasePlayerState;

    private Vector3[] path;
    private int targetIndex;

    [HideInInspector] public bool isSwitch;
    public Vector3 currentWaypoint;

    void Start()
	{
        player = observer.player;
        playerHide = player.GetComponent<Hide>();
		patrolState = new PatrolState(this);
        goingToSoundState = new GoingToSoundState(this);
        chasePlayerState = new ChasePlayerState(this);
		transform.position = waypointsToPatrol[0].position;
		SetState(patrolState);
	}

	void Update()
	{
        if (currentState != chasePlayerState && observer.CheckIfPlayerIsInRange())
        {
            SetState(chasePlayerState);
        }
		currentState.UpdateState();
	}

	public void SetState(State nextState)
	{
		if(currentState != null)
			currentState.OnStateExit();
		currentState = nextState;
		currentState.OnStateEnter();
	}

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = newPath;
            targetIndex = 0;
            if(path.Length > 0)
            {
                StopCoroutine("FollowPath"); //This is for stopping the coroutine in case it's already running
                StartCoroutine("FollowPath");

            }
        }
    }

    private IEnumerator FollowPath()
    {
        currentWaypoint = path[0];
        while (true)
        {
            if (transform.position == currentWaypoint)
            {
                targetIndex++;
                if (targetIndex >= path.Length) //Path finished
                {
                    currentState.WaypointReached();
                    yield break;
                }
                currentWaypoint = path[targetIndex];
            }
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(currentWaypoint - transform.position), rotationSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public void OnSoundDetected(Vector3 soundPosition, bool isSwitch)
    {
        this.isSwitch = isSwitch;
        if (isSwitch)
        {
            SetState(chasePlayerState);
        }
        else if(currentState != chasePlayerState)
        {
            currentWaypoint = soundPosition;
            SetState(goingToSoundState);
        }
    }

    public void OnSwitchOff()
    {
        this.isSwitch = false;
        SetState(patrolState);
    }
}