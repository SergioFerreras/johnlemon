﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObserverGargoyle : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform ghostParent;
    [SerializeField] private GameEnding gameEnding;

    [SerializeField] private AuxLightController lightController;

    bool m_IsPlayerInRange;

    void Update()
    {
        if (m_IsPlayerInRange && !lightController.IsOn)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    GetComponentInParent<AudioSource>().Play();
                    lightController.TurnOn();
                    player.GetComponent<Switch>().findPathToSwitch();
                    foreach (Transform ghost in ghostParent)
                        ghost.GetComponent<ISoundTrigger>().OnSoundDetected(player.position, true);
                }
            }

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }
}